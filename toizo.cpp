#include <iostream>
#include <sstream>
#include <bitset>
#include <vector>
#include <memory>

#define BLOCK 63
#define BL_PREFIX 0

#define TG_PREFIX 1
#define ES_PREFIX 2

#define DD_PREFIX 3 // directable device without color (mirrors, pipes)
#define LU_PREFIX 1
#define LP_PREFIX 2
#define LK_PREFIX 3
#define RU_PREFIX 0

#define PREFIX_MASK 192
#define COLOR_MASK 56
#define DIRECTION_MASK 7

#define NOT_REFLECTING 127
#define CALC_DIRECTION 127
#define CALC_COLOR 127

#define RESET_COLOR 248

char** parse_input(std::istream& in_stream, char& W, char& H, char& N, char& LU_unused, char& LK_unused, char& LP_unused, std::vector<char>& lasers, char& targets_count);
char** solve(char** start_board, char W, char H, char N, char LU_unused, char LK_unused, char LP_unused, std::vector<char>& lasers, char targets_count);

void print_output(char** board, char W, char H, char N);

bool test_moves(char** board, char W, char H, char start_x, char start_y, std::vector<char>& lasers,
                char direction, char, char, char LU_unused, char LK_unused, char LP_unused,
                std::vector<char> *board_difff = NULL, char *targets_diff = NULL, char *LU_diff = NULL, char *LK_diff = NULL, char *LP_diff = NULL);

char redirect_beam_LU(char beam_dir, char mirror_dir);
char redirect_beam_LK(char beam_dir, char mirror_dir);
char redirect_beam_LP(char beam_dir, char mirror_dir, bool& pass);
void move(char direction, char &x, char &y);

int main() {
//    std::stringstream strumyk;
//    strumyk<<"5 5\n"
//                     "11\n"
//                     "LP 0 0 0 0\n"
//                     "LK 0 0 0 0 \n"
//                     "LP 0 0 0 0\n"
//                     "ES 4 4 7 100\n"
//                     "ES 1 5 5 010\n"
//                     "TG 2 4 0 110\n"
//                     "TG 3 1 0 100\n"
//                     "TG 5 1 0 010\n"
//                     "RU 5 5 2 0\n"
//                     "RU 4 2 1 0\n"
//                     "BL 4 1 0 0";

    char W, H, N;
    char LK_unused = 0, LU_unused = 0, LP_unused = 0, targets_count = 0;
    std::vector<char> lasers;

    char** start_board = parse_input(std::cin, W, H, N, LU_unused, LK_unused, LP_unused, lasers, targets_count);
    solve(start_board, W, H, N, LU_unused, LK_unused, LP_unused, lasers, targets_count);

    return 0;
}

char** parse_input(std::istream& in_stream, char& W, char& H, char& N, char& LU_unused, char& LK_unused, char& LP_unused, std::vector<char>& lasers, char& targets_count) {
    short _W, _H, _N;
    in_stream>>_W>>_H>>_N;
    W = (char) _W;
    H = (char) _H;
    N = (char) _N;

    char** board = new char*[W];
    for(int i=0; i<W; i++) {
        board[i] = new char[H];
        for(int j=0; j<H; j++){
            board[i][j] = 0;
        }
    }

    // parse input
    std::string prefix, color;
    short x, y, direction;

    char field_value = 0;
    for(int i=0; i<N; i++) {
        in_stream>>prefix>>x>>y>>direction>>color;

        x--;
        y--;

        std::bitset<3> color_b(color);

        if(prefix == "BL") {
            field_value = BLOCK;
        } else if(prefix == "TG"){
            field_value = TG_PREFIX;
            field_value = field_value << 3;
            field_value = field_value | (char)color_b.to_ulong();
            field_value = field_value << 3;

            targets_count++;
        } else if(prefix == "ES"){
            field_value = ES_PREFIX;
            field_value = field_value << 3;
            field_value = field_value | (char)color_b.to_ulong();
            field_value = field_value << 3;
            field_value = field_value | (char)direction;

            lasers.push_back(x);
            lasers.push_back(y);
            lasers.push_back((char) color_b.to_ulong());
            lasers.push_back(direction);
        } else {
            field_value = DD_PREFIX;
            field_value = field_value << 3;

            if(prefix == "LU"){
                if(x<0 || y<0){
                    LU_unused++;
                    continue;
                }
                field_value = field_value | (char)LU_PREFIX;
            } else if(prefix == "LP"){
                if(x<0 || y<0){
                    LP_unused++;
                    continue;
                }
                field_value = field_value | (char)LP_PREFIX;
            } else if(prefix == "LK"){
                if(x<0 || y<0){
                    LK_unused++;
                    continue;
                }
                field_value = field_value | (char)LK_PREFIX;
            }
            else if(prefix == "RU") field_value = field_value | (char)RU_PREFIX;

            field_value = field_value << 3;
            field_value = field_value | (char)direction;
        }

        board[x][y] = field_value;
    }

    return board;
}

void print_output(char** board, char W, char H, char N){
    std::cout<<(short)W<<" "<<(short)H<<std::endl<<(short)N<<std::endl;

    for(int i=0; i<W; i++){
        for(int j=0; j<H; j++){
            char field_value = board[i][j];
            if(!field_value) continue;

            char prefix = (char) ((field_value & PREFIX_MASK) >> 6);
            char color = (char) ((field_value & COLOR_MASK) >> 3);
            std::bitset<3> color_b(color);
            char direction = (char) (field_value & DIRECTION_MASK);

            switch (prefix){
                case BL_PREFIX: {
                    std::cout << "BL " << i+1 << " " << j+1 << " " << 0 << " " << 0 << std::endl;
                    break;
                } case TG_PREFIX: {
                    std::cout << "TG " << i+1 << " " << j+1 << " " << 0 << " " << color_b << std::endl;
                    break;
                } case ES_PREFIX: {
                    std::cout << "ES " << i+1 << " " << j+1 << " " << (int) direction << " " << color_b << std::endl;
                    break;
                } case DD_PREFIX: {
                    switch (color){
                        case LU_PREFIX: {
                            std::cout << "LU ";
                            break;
                        } case LK_PREFIX: {
                            std::cout << "LK ";
                            break;
                        } case LP_PREFIX: {
                            std::cout << "LP ";
                            break;
                        } case RU_PREFIX: {
                            std::cout << "RU ";
                            break;
                        }
                        default:
                            break;
                    }
                    std::cout << i+1 << " " << j+1 << " " << (int) direction << " " << 0 << std::endl;
                    break;
                }
                default:
                    break;
            }

            std::cout.flush();
        }
    }
}

char** solve(char** start_board, char W, char H, char N, char LU_unused, char LK_unused, char LP_unused, std::vector<char>& lasers, char targets_count){

    // for each light source
    for(std::vector<char>::iterator it = lasers.begin(); it != lasers.end(); it+=4){
        char x_l = *it;
        char y_l = *(it+1);
        char col = *(it + 2);
        char dir = *(it+3);

        *it = -1;
        *(it + 1) = -1;
        *(it+2) = -1;
        *(it+3) = -1;

        std::vector<char> woda_i_lasery(lasers);

        *it = x_l;
        *(it + 1) = y_l;
        *(it+2) = col;
        *(it+3) = dir;

        if(test_moves(start_board, W, H, x_l, y_l, woda_i_lasery, dir, col, targets_count, LU_unused, LK_unused, LP_unused)){
            print_output(start_board, W, H, N);
            std::cout.flush();

            break;
        }
    }

    return NULL;
}

bool test_moves(char** board, char W, char H, char start_x, char start_y, std::vector<char>& _lasers,
                char start_dir, char start_color, char targets_left,
                char LU_unused, char LK_unused, char LP_unused, std::vector<char> *board_difff, char *targets_diff, char* LU_diff, char* LK_diff, char* LP_diff){

    std::vector<char> checked_targets;
    std::vector<char> lasers(_lasers);

    char start_src = board[start_x][start_y];

    char color;
    if(start_color == CALC_COLOR) {
        color = (char) ((start_src & COLOR_MASK) >> 3);
    }
    else {
        color = start_color;
    }

    char direction;
    if(start_dir == CALC_DIRECTION) {
        direction = (char) (start_src & DIRECTION_MASK);
    }
    else {
        direction = start_dir;
    }

    char x = start_x, y = start_y;
    while(true){
        move(direction, x, y);

        char field_value = 0;

        // validate move
        if(x < 0 || y < 0 || x >= W || y >= H) break;
        else field_value = board[x][y];

        // if beam hits either: block, pipe or mirror incorrectly directed...
        char prefix = (char) ((field_value & PREFIX_MASK) >> 6);
        if(field_value == BLOCK || prefix == ES_PREFIX) break;

        if(prefix == TG_PREFIX){
            char expected_color = (char) ((field_value & COLOR_MASK) >> 3);
            char actual_color = (char) (field_value & DIRECTION_MASK);
            char new_color = actual_color | color;

            if(expected_color == new_color && actual_color != expected_color){
                targets_left--;
                if(!targets_left) return true;
            } else if (expected_color != new_color && actual_color == expected_color) {
                targets_left++;
            }

            checked_targets.push_back(x);
            checked_targets.push_back(y);
            checked_targets.push_back(field_value);

            field_value = (char) (field_value & RESET_COLOR);
            field_value = field_value | new_color;

            board[x][y] = field_value;
        }
            // check mirrors, redirect beam if necessary
        else if(prefix == DD_PREFIX) {
            prefix = (char) ((field_value & COLOR_MASK) >> 3);

            char mirror_direction = (char) (field_value & DIRECTION_MASK);
            if(prefix == LU_PREFIX){
                direction = redirect_beam_LU(direction, mirror_direction);
            } else if(prefix == LK_PREFIX){
                direction = redirect_beam_LK(direction, mirror_direction);
            } else if(prefix == LP_PREFIX) {
                bool pass_beam;
                char old_dir = direction;
                direction = redirect_beam_LP(direction, mirror_direction, pass_beam);

                std::vector<char> brd_diff;
                char _trg_diff = 0, _LP_diff = 0, _LK_diff = 0, _LU_diff = 0;

                if(pass_beam) {
                    bool ret_val = test_moves(board, W, H, x, y, lasers, old_dir, color, targets_left, LU_unused,
                                              LK_unused, LP_unused, &brd_diff, &_trg_diff, &_LU_diff, &_LK_diff,
                                              &_LP_diff);
                    if (ret_val) return true;

                    for(std::vector<char>::iterator it = brd_diff.begin(); it != brd_diff.end(); ++it){
                        checked_targets.push_back(*it);
                    }

                    targets_left = _trg_diff;
                    LU_unused = _LU_diff;
                    LK_unused = _LK_diff;
                    LP_unused = _LP_diff;
                }
            }
            else if(prefix == RU_PREFIX && direction != mirror_direction && direction != mirror_direction - 4) direction = NOT_REFLECTING;

            if(direction == NOT_REFLECTING) break;
        }

        else if(field_value == 0) {

            // place nothing
            bool ret_vale = test_moves(board, W, H, x, y, lasers, direction, color, targets_left, LU_unused, LK_unused,
                                       LP_unused);
            if (ret_vale) return true;

            if (LU_unused) {
                char lu_stro = (char) (((DD_PREFIX << 3) | LU_PREFIX) << 3);

                for (int i = 0; i < 8; i++) {
                    char new_dir = redirect_beam_LU(direction, i);
                    if (new_dir != NOT_REFLECTING) {
                        board[x][y] = lu_stro | (char) i;

                        // follow the beam!
                        bool ret_val = test_moves(board, W, H, x, y, lasers, new_dir, color, targets_left,
                                                  LU_unused - 1, LK_unused, LP_unused);
                        if (ret_val) return true;

                        board[x][y] = 0;
                    }
                }
            }

            if (LK_unused) {
                char lu_stro = (char) (((DD_PREFIX << 3) | LK_PREFIX) << 3);

                for (int i = 0; i < 8; i++) {
                    char new_dir = redirect_beam_LK(direction, i);
                    if (new_dir != NOT_REFLECTING) {
                        board[x][y] = lu_stro | (char) i;

                        // follow the beam!
                        bool ret_val = test_moves(board, W, H, x, y, lasers, new_dir, color, targets_left, LU_unused,
                                                  LK_unused - 1, LP_unused);
                        if (ret_val) return true;

                        board[x][y] = 0;
                    }
                }
            }

            if (LP_unused) {
                char lu_stro = (char) (((DD_PREFIX << 3) | LP_PREFIX) << 3);

                for(int i = 0; i < 4; i++){
                    bool pass;
                    char new_dir = redirect_beam_LP(direction, i, pass);

                    if(new_dir != NOT_REFLECTING){
                        board[x][y] = lu_stro | (char) i;

//                        std::vector<char> brd_dff;
//                        char trg_diff = 0, _LU_diff = 0, _LK_diff = 0, _LP_diff = 0;

//                        bool ret_val = test_moves(board, W, H, x, y, lasers, new_dir, color, targets_left, LU_unused, LK_unused, LP_unused - 1, &brd_dff, &trg_diff, &_LU_diff, &_LK_diff, &_LP_diff);
//                        if(ret_val) return true;
                        lasers.push_back(x);
                        lasers.push_back(y);
                        lasers.push_back(color);
                        lasers.push_back(direction);

                        bool ret_val = test_moves(board, W, H, x, y, lasers, new_dir, color, targets_left, LU_unused, LK_unused, LP_unused - 1);
                        if(ret_val) return true;

//                        for (std::vector<char>::iterator it = brd_dff.begin(); it != brd_dff.end(); it += 3) {
//                            char old_x = *it, old_y = *(it + 1), old_value = *(it + 2);
//                            board[old_x][old_y] = old_value;
//                        }

                        lasers.pop_back();
                        lasers.pop_back();
                        lasers.pop_back();
                        lasers.pop_back();

                        board[x][y] = 0;
                    } else if (new_dir == NOT_REFLECTING && pass){
                        board[x][y] = lu_stro | (char) i;

                        bool ret_val = test_moves(board, W, H, x, y, lasers, direction, color, targets_left, LU_unused, LK_unused, LP_unused - 1);
                        if(ret_val)
                            return true;
                    }
                }
            }



//            board[x][y] = 0;
        }

        // go along beam
        char new_dir = direction;
        char tmp_x = x, tmp_y = y;
        std::vector<char> board_diff;
        char nwe_trg_left = targets_left;
        while (true) {
            move(direction, tmp_x, tmp_y);
            if (tmp_x < 0 || tmp_y < 0 || tmp_x >= W || tmp_y >= H) break;

            char tg_field_value = board[tmp_x][tmp_y];
            char prefix = (char) ((tg_field_value & PREFIX_MASK) >> 6);
            if (prefix == TG_PREFIX) {
                char expected_color = (char) ((tg_field_value & COLOR_MASK) >> 3);
                char actual_color = (char) (tg_field_value & DIRECTION_MASK);
                char new_color = actual_color | color;

                if (expected_color == new_color && actual_color != expected_color) {
                    nwe_trg_left--;
                } else if (expected_color != new_color && actual_color == expected_color) {
                    nwe_trg_left++;
                }

                board_diff.push_back(tmp_x);
                board_diff.push_back(tmp_y);
                board_diff.push_back(tg_field_value);

                tg_field_value = (char) (tg_field_value & RESET_COLOR);
                tg_field_value = tg_field_value | new_color;

                board[tmp_x][tmp_y] = tg_field_value;
                if (!nwe_trg_left) return true;
            }

            else if (prefix == DD_PREFIX) {
                prefix = (char) ((tg_field_value & COLOR_MASK) >> 3);

                char mirror_direction = (char) (tg_field_value & DIRECTION_MASK);
                if (prefix == LU_PREFIX) {
                    new_dir = redirect_beam_LU(new_dir, mirror_direction);
                } else if (prefix == LK_PREFIX) {
                    new_dir = redirect_beam_LK(new_dir, mirror_direction);
                } else if(prefix == LP_PREFIX) {
                    bool pass_beam;
                }

                if (new_dir == NOT_REFLECTING) break;
            }
        }

        for (std::vector<char>::iterator ite = lasers.begin(); ite != lasers.end(); ite += 4) {
            if (*ite == -1) continue;

            char new_laser_x = *ite;
            char new_laser_y = *(ite + 1);
            char new_laser_col = *(ite + 2);
            char new_laser_dir = *(ite + 3);

            *ite = -1;
            *(ite + 1) = -1;
            *(ite + 2) = -1;
            *(ite + 3) = -1;

            std::vector<char> lasery(lasers);

            *ite = new_laser_x;
            *(ite + 1) = new_laser_y;
            *(ite + 2) = new_laser_col;
            *(ite + 3) = new_laser_dir;


            bool val = test_moves(board, W, H, new_laser_x, new_laser_y, lasery, new_laser_dir, new_laser_col,
                                  nwe_trg_left, LU_unused, LK_unused, LP_unused);
            if (val) return true;
        }

        for (std::vector<char>::iterator it = board_diff.begin(); it != board_diff.end(); it += 3) {
            char old_x = *it, old_y = *(it + 1), old_value = *(it + 2);
            board[old_x][old_y] = old_value;
        }

    }

    // restore board state before function call

    if(board_difff){
        *board_difff = checked_targets;
        *targets_diff = targets_left;
        *LP_diff = LP_unused;
    } else {
        for (std::vector<char>::iterator it = checked_targets.end(); it != checked_targets.begin(); it -= 3) {
            board[*(it - 3)][*(it - 2)] = *(it - 1);
        }
    }

    return false;
}

char redirect_beam_LU(char beam_dir, char mirror_dir){
    char expected_mirror = (char) ((beam_dir + 3) % 8);
    if(expected_mirror == mirror_dir)
        return (char) ((beam_dir + 2) % 8);

    expected_mirror = (char) (((beam_dir - 3) % 8 + 8) % 8);
    if(expected_mirror == mirror_dir)
        return (char) (((beam_dir - 2) % 8 + 8) % 8);

    return NOT_REFLECTING;
}

char redirect_beam_LK(char beam_dir, char mirror_dir){
    char expected_mirror = (char) (((beam_dir - 4) % 8 + 8) % 8);
    if(expected_mirror == mirror_dir)
        return (char) ((beam_dir + 3) % 8);

    expected_mirror = (char) (((beam_dir - 3) % 8 + 8) % 8);
    if(expected_mirror == mirror_dir)
        return mirror_dir;

    expected_mirror = (char) (((beam_dir - 2) % 8 + 8) % 8);
    if(expected_mirror == mirror_dir)
        return (char) ((mirror_dir + 1) % 8);

    expected_mirror = (char) ((beam_dir + 3) % 8);
    if(expected_mirror == mirror_dir)
        return (char) (((mirror_dir - 2) % 8 + 8) % 8);

    return NOT_REFLECTING;
}

char redirect_beam_LP(char beam_dir, char mirror_dir, bool& pass){

    char new_dir = redirect_beam_LU(beam_dir, mirror_dir);
    if(new_dir == NOT_REFLECTING) new_dir = redirect_beam_LU(beam_dir, (mirror_dir + 4) % 8);

    if(new_dir != NOT_REFLECTING) pass = true;
    else {
        pass = false;
        if (beam_dir == mirror_dir) pass = true;
        else {
            char expected_mirror = (char) ((beam_dir + 4) % 8);
            if (expected_mirror == mirror_dir) pass = true;
        }
    }

    return new_dir;
}

void move(char direction, char &x, char &y){
    switch(direction) {
        case 0:
            x--;
            break;
        case 1:
            x--;
            y++;
            break;
        case 2:
            y++;
            break;
        case 3:
            x++;
            y++;
            break;
        case 4:
            x++;
            break;
        case 5:
            x++;
            y--;
            break;
        case 6:
            y--;
            break;
        case 7:
            x--;
            y--;
            break;
        default:
            break;
    }
}